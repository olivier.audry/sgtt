#!/usr/bin/env python
import sys
import gitlab
import pickle
from datetime import datetime
from pytz import timezone
import shortuuid
import os


if __name__ == "__main__":

    project = sys.argv[1]
    issue = sys.argv[2]
    cmd = sys.argv[3]
    msg = sys.argv[4] if len(sys.argv) == 5 else None

    if not os.path.exists("/home/oaudry/.config/sgtt/"):
        os.makedirs("/home/oaudry/.config/sgtt/")

    try:
        store = pickle.load(
            open("/home/oaudry/.config/sgtt/store.pickle", "rb")
        )
    except (OSError, IOError):
        store = {}

    if project not in store:
        store[project] = {}
    if issue not in store[project]:
        store[project][issue] = []

    if cmd == "start":
        if not len(store[project][issue]) or store[project][issue][-1]["end"]:
            store[project][issue].append(
                {
                    "id": shortuuid.uuid(),
                    "start": datetime.now(timezone("Europe/Paris")),
                    "end": None,
                    "sync": False,
                    "msg": msg,
                }
            )
        else:
            print("Cannot start a non ended period")
    elif cmd == "stop":
        _d = store[project][issue][-1]
        if _d["start"]:
            if _d["end"]:
                print("Cannot end an ended period")
            else:
                _d["end"] = datetime.now(timezone("Europe/Paris"))
                _d["msg"] = msg
                store[project][issue][-1] = _d
        else:
            print("No start found. Cannot end witout a start")
            exit(-1)
    elif cmd == "ls":
        for k, s in enumerate(store[project][issue]):
            if "sync" in s and s["sync"]:
                continue
            if s["end"]:
                end = s["end"]
            else:
                end = datetime.now(timezone("Europe/Paris"))

            h, r = divmod((end - s["start"]).total_seconds(), 3600)
            m, _ = divmod(r, 60)
            if m == h == 0:
                duration = "n.a"
            else:
                duration = "%ih%im" % (int(h), int(m))
            print(
                s["id"],
                duration,
                "spend on",
                project,
                "for issue",
                issue,
                "synced to gitlab" if s["sync"] else "not synced to gitlab",
            )
    elif cmd == "rm":
        for k, s in enumerate(store[project][issue]):
            if s["id"] == msg:
                store[project][issue].pop(k)

    elif cmd == "sync":
        try:
            gl = gitlab.Gitlab.from_config("gitlab")
            gl.auth()
        except Exception as e:
            print("login issue:", str(e))
            exit(-1)

        try:
            _project = gl.projects.get(project)
        except Exception as e:
            print("project issur", str(e))
            exit(-1)

        try:
            _issue = _project.issues.get(int(issue))
        except Exception as e:
            print("issue issue", str(e), issue)
            exit(-1)

        for k, s in enumerate(store[project][issue]):
            if "sync" in s and s["sync"]:
                continue
            h, r = divmod((s["end"] - s["start"]).total_seconds(), 3600)
            m, s = divmod(r, 60)
            if m == h == 0:
                pass
            else:
                duration = "%ih%im" % (int(h), int(m))
                try:
                    _issue.add_spent_time(duration)
                except Exception as e:
                    print("Cannot add spend time", str(e))
                    continue
                print("added ", duration, "to", project, "issue", issue)
                store[project][issue][k]["sync"] = True
    else:
        exit(-1)

    pickle.dump(store, open("/home/oaudry/.config/sgtt/store.pickle", "wb"))
