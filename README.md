# sgtt

Simple gitlab time tracker

Configure ~/.python-gitlab.cfg
[gitlab]
url = https://gitlab.com
private_token = YOUR_PRIVATE_TOKEN
api_version = 4


start period : ./sgtt.py "project/subproject" issue start 'some optional message'
stop period : ./sgtt.py "project/subproject" issue stop 'some optional message'
add spend time to issue : ./sgtt.py "project/subproject" issue sync
